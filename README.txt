This package contains scripts to control the player movements and the camera

---Move the player---
Add a "Movement Manager" to a gameobject and reference a "Character controller"

--Move by inputs
Add a "Input Movement" to a gameobject and reference the "Movement Manager", you must the 
call the "ReceiveInputs(Vector3)" method and pass the input parameters.
Note that this script is meant to be used on a topdown game, therefore the Y axis on the 
inputs will be converted to the Z axis for the movement.

--Push the character
Add a "Push Movement" to a gameobject and reference the "Movement Manager", you must calll the 
"Push(Vector3, float, float)" method to push the character. The parameters are respectively
the direction, force and deceleration of the push

--To create another movement effect, create a class that inherits "Movement Effect", and modify the variable pMovementValue, it will be read by the "Movement Manager" and added to the sum of all the movements.

---Rotate the player---
Add the script "LookAtMouse" to an object and reference the object that should look at the mouse. Create a layer for the ground and reference it. 

---Blend the camera to the mouse position---
Create an empty gameobject child to the object you want the camera to follow. Add the script "CameraTargetMovement" and reference this gameobject. In the cinemachine Virtual Camera, set the "Follow" to the same gameobject you have referenced.