﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCWallCheck : MonoBehaviour
    {
        [SerializeField] private LayerMask wallMask;
        [SerializeField] private float minDistanceFromWall;

        public bool CheckWall(Vector3 direction)
        {
            return Physics.Raycast(transform.position, direction, minDistanceFromWall, wallMask);
        }
    }
}
