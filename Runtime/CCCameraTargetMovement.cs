﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCameraTargetMovement : MonoBehaviour
    {
        [SerializeField] private float blendForce;
        [SerializeField] private Transform cameraTarget;
        private Vector2 screenCenter = new Vector2(0.5f, 0.5f);
        private Camera cam;

        public void SetTarget(Transform target) { cameraTarget = target; }

        private void Start()
        {
            cam = Camera.main;
        }

        private void Update()
        {
            MoveTarget();
        }

        private void MoveTarget()
        {
            Vector2 viewportMousePos = cam.ScreenToViewportPoint(Input.mousePosition);
            Vector2 relativeMousePos = viewportMousePos - screenCenter;

            Vector3 newTargetPosition = new Vector3(
                relativeMousePos.x * blendForce,
                0,
                relativeMousePos.y * blendForce
                );

            cameraTarget.transform.localPosition = newTargetPosition;
        }
    }
}
