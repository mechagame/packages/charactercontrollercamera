﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCMovementManager : MonoBehaviour
    {
        [SerializeField] private CharacterController controller;
        [SerializeField] private CCCWallCheck wallCheck;
        private List<CCCMovementEffect> movementEffects = new List<CCCMovementEffect>();
        private Vector3 currentMovement;
        public Vector3 GetCurrentMovement() => currentMovement;

        public void AddEffect(CCCMovementEffect effect) => movementEffects.Add(effect);
        public void RemoveEffect(CCCMovementEffect effect) => movementEffects.Remove(effect);

        private void Update()
        {
            ProcessMovement();
        }

        private void ProcessMovement()
        {
            Vector3 _moveIntention = Vector3.zero;
            for (int i = 0; i < movementEffects.Count; i++)
            {
                _moveIntention += movementEffects[i].GetMovementValue;
            }
            currentMovement = _moveIntention;
            if (wallCheck.CheckWall(currentMovement.normalized)) { return; }
            controller.Move(currentMovement * Time.deltaTime);
        }
    }
}
