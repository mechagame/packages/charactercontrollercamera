﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCSpeedModifier : MonoBehaviour
    {
        [SerializeField] private CCCInputMovement movement;
        [SerializeField] private float speedBoost;
        [SerializeField] private float boostDuration;
        private float baseSpeed;

        private void Start()
        {
            baseSpeed = movement.GetMovementSpeed;
        }

        public void ModifySpeed()
        {
            StartCoroutine(UpdateSpeed());
        }

        public IEnumerator UpdateSpeed()
        {
            movement.SetMovementSpeed(baseSpeed + speedBoost);
            yield return new WaitForSeconds(boostDuration);
            movement.SetMovementSpeed(baseSpeed);
        }
    }
}
