﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCBumpMovement : MonoBehaviour
    {
        [SerializeField] private CCCPushMovement push;
        [SerializeField] private float decelerationForce;

        private void OnTriggerEnter(Collider other)
        {
            if(!other.TryGetComponent(out CCCBumper bumper)) { return; }

            Vector3 pushDirection = (transform.position - bumper.transform.position).normalized;
            if(pushDirection == Vector3.zero) { pushDirection = transform.forward; }
            pushDirection = new Vector3(pushDirection.x, 0, pushDirection.y);
            float pushForce = bumper.GetPushForce;
            push.Push(pushDirection, pushForce, decelerationForce);
        }
    }
}
