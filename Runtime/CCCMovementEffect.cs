﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public abstract class CCCMovementEffect : MonoBehaviour
    {
        [SerializeField] private CCCMovementManager movementManager;
        protected Vector3 pMovementValue;
        public Vector3 GetMovementValue => pMovementValue;

        protected virtual void Start() => movementManager.AddEffect(this);
        protected virtual void OnDestroy() => movementManager.RemoveEffect(this);
    }
}