﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCCBumper : MonoBehaviour
{
    [SerializeField] private float pushForce;
    public float GetPushForce => pushForce;
}
