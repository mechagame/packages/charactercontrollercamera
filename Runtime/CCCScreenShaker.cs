﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCScreenShaker : MonoBehaviour
    {
        [SerializeField] private float shakeForce = 1f;
        [SerializeField] private float shakeTime = 0.5f;

        public void Shake()
        {
            CCCScreenShakerManager.GetInstance.ShakeScreen(shakeForce, shakeTime);
        }
    }
}
