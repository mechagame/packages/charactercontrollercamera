﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCInputMovement : CCCMovementEffect
    {
        [Header("Settings")]
        [SerializeField] private float movementSpeed;
        [SerializeField] private float acceleration;
        [SerializeField] private float worldYRotation;

        public float GetMovementSpeed => movementSpeed;
        public void SetMovementSpeed(float newSpeed) => movementSpeed = newSpeed;
        public float GetAcceleration => acceleration;
        public void SetAcceleration(float newAcceleration) => acceleration = newAcceleration;

        private float currentMovementSpeed;
        private Vector3 lastMovementInput;

        public void ReceiveInputs(Vector3 inputMovement)
        {
            float _targetMovementSpeed = movementSpeed;

            if (inputMovement == Vector3.zero)
            {
                _targetMovementSpeed = 0;
                inputMovement = lastMovementInput;
            }
            else
            {
                inputMovement = Quaternion.AngleAxis(worldYRotation, Vector3.up) * inputMovement;
                lastMovementInput = inputMovement;
            }

            currentMovementSpeed = Mathf.MoveTowards(currentMovementSpeed, _targetMovementSpeed, acceleration * Time.deltaTime);
            pMovementValue = inputMovement.normalized * currentMovementSpeed;
        }
    }
}
