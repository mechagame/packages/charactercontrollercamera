﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCLookAtMouse : MonoBehaviour
    {
        [SerializeField] private LayerMask groundLayer;
        [SerializeField] private GameObject body;
        [SerializeField] private float characterHeight;
        private Camera cam;
        private Vector3 lastPosition;

        private void Start()
        {
            cam = Camera.main;
            characterHeight = body.transform.localPosition.y;
        }

        private void Update()
        {
            RotateCharacter();
        }

        private void RotateCharacter()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hitInfo, Mathf.Infinity, groundLayer))
            {
                lastPosition = new Vector3(hitInfo.point.x,hitInfo.point.y + characterHeight, hitInfo.point.z);
            }
            body.transform.LookAt(lastPosition);
        }
    }
}
