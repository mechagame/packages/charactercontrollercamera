﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCPushMovement : CCCMovementEffect
    {
        private Vector3 currentPushDirection;
        private float currentPushForce;
        private float currentDeceleration;

        public void Push(Vector3 direction, float force, float deceleration)
        {
            currentPushDirection = direction;
            currentPushForce = force;
            currentDeceleration = deceleration;
        }

        private void Update()
        {
            ProcessPush();
        }

        private void ProcessPush()
        {
            if (currentPushForce <= 0) { return; }

            currentPushForce = Mathf.MoveTowards(currentPushForce, 0, currentDeceleration * Time.deltaTime);
            pMovementValue = currentPushDirection * currentPushForce;
        }
    }
}
