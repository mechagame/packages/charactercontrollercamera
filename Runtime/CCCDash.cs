﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterControllerCamera
{
    public class CCCDash : MonoBehaviour
    {
        [SerializeField] private CCCPushMovement pushMovement;
        [SerializeField] private GameObject dashDirectionObject;
        [SerializeField] private float dashForce;
        [SerializeField] private float dashDeceleration;

        public void Dash()
        {
            pushMovement.Push(dashDirectionObject.transform.forward, dashForce, dashDeceleration);
        }

    }
}
