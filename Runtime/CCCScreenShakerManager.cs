﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

namespace CharacterControllerCamera
{
    public class CCCScreenShakerManager : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera cam;
        private CinemachineBasicMultiChannelPerlin perlin;
        private static CCCScreenShakerManager instance;
        public static CCCScreenShakerManager GetInstance => instance;
        private float shakeTimer;
        private float shakeTotalTime;
        private float startingIntensity;

        private void Awake()
        {
            SetAsSingleton();
            perlin = cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        private void SetAsSingleton()
        {
            if (instance)
            {
                Destroy(this);
            }
            else
            {
                instance = this;
            }
        }

        public void ShakeScreen(float amplitude, float time)
        {
            perlin.m_AmplitudeGain = amplitude;
            shakeTimer = 0;
            shakeTotalTime = time;
            startingIntensity = amplitude;
        }

        private void Update()
        {
            if (shakeTimer < shakeTotalTime)
            {
                shakeTimer += Time.deltaTime;
                float currentShake = Mathf.Lerp(startingIntensity, 0f, shakeTimer / shakeTotalTime);
                perlin.m_AmplitudeGain = currentShake;
            }
        }
    }
}
