﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCCYClamp : MonoBehaviour
{
    [SerializeField] private float lockedY;

    private void LateUpdate()
    {
        transform.position = new Vector3(transform.position.x, lockedY, transform.position.z);
    }
}
